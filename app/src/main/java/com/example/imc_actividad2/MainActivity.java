package com.example.imc_actividad2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class MainActivity extends AppCompatActivity {
    private EditText txtNumero1;
    private EditText txtNumero2;
    private EditText txtNumero3;
    private Button button;
    private Button buttonLimpiar;
    private Button buttonCerrar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_main);

        txtNumero1 = findViewById(R.id.txtNumero1);
        txtNumero2 = findViewById(R.id.txtNumero2);
        txtNumero3 = findViewById(R.id.txtNumero3);
        button = findViewById(R.id.button);
        buttonLimpiar = findViewById(R.id.buttonLimpiar);
        buttonCerrar = findViewById(R.id.buttonCerrar);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imc();
            }
        });

        buttonLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        buttonCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cerrarAplicacion();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    private void imc() {
        String numero1Txt = txtNumero1.getText().toString();
        String numero2Txt = txtNumero2.getText().toString();
        String numero3Txt = txtNumero3.getText().toString();

        if (numero1Txt.isEmpty() || numero2Txt.isEmpty() || numero3Txt.isEmpty()) {
            Toast.makeText(getApplicationContext(), "Por favor llene todos los datos", Toast.LENGTH_SHORT).show();
        } else {
            int edad = Integer.parseInt(numero1Txt);
            float peso = Float.parseFloat(numero2Txt);
            float estatura = Float.parseFloat(numero3Txt);

            if (estatura == 0) {
                Toast.makeText(getApplicationContext(), "La estatura no puede ser cero", Toast.LENGTH_SHORT).show();
                return;
            }

            // Calcular el índice de masa corporal
            float imc = peso / (estatura * estatura);
            // Convertir el resultado a texto
            String imcTexto = String.format("%.1f", imc);

            String mensaje;
            if (imc < 18.5f) {
                mensaje = "Usted tiene desnutrición";
            } else if (imc >= 18.5f && imc < 25) {
                mensaje = "Usted está normal";
            } else if (imc >= 25 && imc < 30) {
                mensaje = "Usted tiene sobrepeso";
            } else if (imc >= 30 && imc < 35) {
                mensaje = "Usted tiene obesidad grado 1";
            } else if (imc >= 35 && imc < 40) {
                mensaje = "Usted tiene obesidad grado 2";
            } else {
                mensaje = "Usted tiene obesidad grado 3";
            }

            Toast.makeText(getApplicationContext(), "Su edad es de " + edad + " años y su masa corporal es de: " + imcTexto + ". " + mensaje, Toast.LENGTH_LONG).show();
        }
    }

    private void limpiarCampos() {
        txtNumero1.setText("");
        txtNumero2.setText("");
        txtNumero3.setText("");
        Toast.makeText(getApplicationContext(), "Campos limpiados", Toast.LENGTH_SHORT).show();
    }

    private void cerrarAplicacion() {
        finish();
        System.exit(0);
    }
}
