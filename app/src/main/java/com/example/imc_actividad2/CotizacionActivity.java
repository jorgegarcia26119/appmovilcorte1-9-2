package com.example.imc_actividad2;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class CotizacionActivity extends AppCompatActivity {

    private TextView lblNombre, lblFolio;
    private EditText txtDescripcion,txtValorAuto,txtPorcentaje;
    private RadioButton rdb12,rdb24,rdb36,rdb48;
    private TextView lblPagoInicial,lblPagoMensual;
    private Button btnCalcular,btnLimpiar,btnRegresar;
    private Cotizacion cot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_cotizacion);

        iniciarComponentes();

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                //Validar

                if(txtDescripcion.getText().toString().matches("") ||
                    txtValorAuto.getText().toString().matches("") ||
                            txtPorcentaje.getText().toString().matches("")){

                    Toast.makeText(CotizacionActivity.this,
                            "Falto Captuar La Informacion", Toast.LENGTH_SHORT).show();

                }else{

                    //Asignar los datos del obgeto cotizacion

                    int plazo = 0;
                    float enganche = 0.0f;
                    float pagoMensual = 0.0f;

                    if(rdb12.isChecked())plazo=12;
                    if(rdb24.isChecked())plazo=24;
                    if(rdb36.isChecked())plazo=36;
                    if(rdb48.isChecked())plazo=48;
                    cot.setDescripcion(txtDescripcion.getText().toString());
                    cot.setValorAuto(Float.parseFloat(txtValorAuto.getText().toString()));
                    cot.setPorEnganche(Float.parseFloat(txtPorcentaje.getText().toString()));
                    cot.setPlazos(plazo);

                    //obtener los calculos

                    enganche = cot.calcularPagoInicial();
                    pagoMensual = cot.calcularPagoMensual();


                    lblPagoInicial.setText(String.valueOf("Pago inicial : $"+ enganche));
                    lblPagoMensual.setText(String.valueOf("Pago Mensual : $"+ pagoMensual));
                }

            }

        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });

        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                System.exit(0);
            }
        });


    }
    public void iniciarComponentes(){

        lblNombre = (TextView) findViewById(R.id.txtUsuario);
        lblFolio = (TextView) findViewById(R.id.txtFolio);

        txtDescripcion = (EditText)  findViewById(R.id.txtDescripcion);
        txtValorAuto = (EditText)  findViewById(R.id.txtValorAuto);
        txtPorcentaje = (EditText)  findViewById(R.id.txtPorPagoInicial);

        rdb12 = (RadioButton) findViewById(R.id.rdb12);
        rdb24 = (RadioButton) findViewById(R.id.rdb24);
        rdb36 = (RadioButton) findViewById(R.id.rdb36);
        rdb48 = (RadioButton) findViewById(R.id.rdb48);

        btnCalcular = (Button) findViewById(R.id.btnCalcular);
        btnLimpiar = (Button) findViewById(R.id.btnLimpiar);
        btnRegresar = (Button) findViewById(R.id.btnRegresar);

        lblPagoInicial = (TextView) findViewById(R.id.txtPagoInicial);
        lblPagoMensual=(TextView) findViewById(R.id.txtPagoMensual);

        cot = new Cotizacion();

        //Mostrar el id
        lblFolio.setText("Folio : " + String.valueOf(cot.generaId()));

        Bundle datos = getIntent().getExtras();
        String nombre = datos.getString("cliente");

        lblNombre.setText(nombre);

        rdb12.setChecked(true);
    }
    private void limpiarCampos() {
        txtDescripcion.setText("");
        txtPorcentaje.setText("");
        txtValorAuto.setText("");

        lblPagoMensual.setText("Pago Inicial: ");
        lblPagoInicial.setText("Pago Mensual: ");

        lblFolio.setText("");
        lblNombre.setText("");
        rdb12.setSelected(true);

        Toast.makeText(getApplicationContext(), "Campos limpiados", Toast.LENGTH_SHORT).show();
    }
}