package com.example.imc_actividad2;

import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.EdgeToEdge;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.Insets;
import androidx.core.view.ViewCompat;
import androidx.core.view.WindowInsetsCompat;

public class monedasActivity extends AppCompatActivity {

    private EditText txtCantidad;
    private TextView txtResultado;
    private Spinner spinner;
    private Button btnCalcular, btnLimpiar, btnCerrar;  // Añadir el botón Cerrar aquí
    private int pos = 0;

    private static final double USD_RATE = 16.70; // Tipo de cambio de MXN a USD
    private static final double CAD_RATE = 12.22; // Tipo de cambio de MXN a CAD
    private static final double EUR_RATE = 18.11; // Tipo de cambio de MXN a EUR
    private static final double GBP_RATE = 21.27; // Tipo de cambio de MXN a Libra

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        EdgeToEdge.enable(this);
        setContentView(R.layout.activity_monedas);
        iniciarComponentes();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                pos = position;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }
        });

        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                realizarConversion();
            }
        });

        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiarCampos();
            }
        });

        // Listener para el botón Cerrar
        btnCerrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish(); // Cierra la actividad
            }
        });

        ViewCompat.setOnApplyWindowInsetsListener(findViewById(R.id.main), (v, insets) -> {
            Insets systemBars = insets.getInsets(WindowInsetsCompat.Type.systemBars());
            v.setPadding(systemBars.left, systemBars.top, systemBars.right, systemBars.bottom);
            return insets;
        });
    }

    public void iniciarComponentes() {
        txtCantidad = findViewById(R.id.txtCantidad);
        txtResultado = findViewById(R.id.txtResultado);
        spinner = findViewById(R.id.spnMoneda);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnCerrar = findViewById(R.id.btnCerrar); // Inicializa el botón Cerrar aquí

        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this,
                R.array.monedas, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void realizarConversion() {
        if (txtCantidad.getText().toString().matches("")) {
            Toast.makeText(getApplicationContext(), "Falto Capturar", Toast.LENGTH_SHORT).show();
            return;
        }

        double cantidadMXN = Double.parseDouble(txtCantidad.getText().toString());
        double resultado = 0;

        switch (pos) {
            case 0: // Pesos a Dólares Americanos
                resultado = cantidadMXN / USD_RATE;
                break;
            case 1: // Pesos a Dólar Canadiense
                resultado = cantidadMXN / CAD_RATE;
                break;
            case 2: // Pesos a Euro
                resultado = cantidadMXN / EUR_RATE;
                break;
            case 3: // Pesos a Libra Esterlina
                resultado = cantidadMXN / GBP_RATE;
                break;
            default:
                break;
        }

        txtResultado.setText(String.format("Resultado: %.2f", resultado));
    }

    private void limpiarCampos() {
        txtCantidad.setText("");
        txtResultado.setText("Resultado: ");
        spinner.setSelection(0);
    }
}
